import path from 'path';
import rollupPreprocessor from '@bahmutov/cy-rollup';

const options = {
  configFile: path.resolve('cypress/rollup.config')
};

export default (on) => {
  on('file:preprocessor', rollupPreprocessor(options));
};
